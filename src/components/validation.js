const validation = p => {
  const { regexp, error, value } = p;
  if (value) {
    return regexp.test(value) ? undefined : error;
  } else {
    return error;
  }
};

export default validation;
