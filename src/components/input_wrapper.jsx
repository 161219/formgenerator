import React from "react";

export const InputWrapper = ({ title, children }) => (
  <div className="input-wrapper">
    <div className="half">
      <span className="title">{title}</span>
    </div>
    <div className="half">
      <div>{children}</div>
    </div>
  </div>
);
