import React from "react";
import { Field } from "react-final-form";
import DatePicker from "react-datepicker";
import validation from "./validation";
import { InputWrapper } from "./input_wrapper";
import { Select } from "./select";

export const MyField = ({ inputProps, mutators }) => (
  <InputWrapper title={inputProps.title}>
    {inputProps.type === "radio" ? (
      inputProps.options.map((x, i) => (
        <label key={i}>
          <Field
            name={inputProps.title}
            component="input"
            type="radio"
            value={x.value}
            {...inputProps}
          />
          {x.value}
          <br />
        </label>
      ))
    ) : (
      <Field
        name={inputProps.title}
        validate={x =>
          inputProps.validation &&
          validation({ ...inputProps.validation, value: x })
        }
        type={inputProps.type}
        render={({ input, meta }) => (
          <>
            {["email", "string", "password", "number", "checkbox"].includes(
              inputProps.type
            ) && (
              <input
                {...input}
                type={inputProps.type}
                component="input"
                placeholder={inputProps.title}
                className={
                  ["checkbox", "radio"].includes(inputProps.type)
                    ? ""
                    : "full-width-input"
                }
              />
            )}
            {inputProps.type === "text" && (
              <textarea {...input} placeholder={inputProps.title} />
            )}
            {inputProps.type === "select" && (
              <Select {...inputProps} input={input} />
            )}
            {["date", "datetime"].includes(inputProps.type) && (
              <DatePicker
                selected={input.value}
                locale="ru"
                placeholderText={inputProps.title}
                dateFormat={inputProps.type === "datetime" ? "Pp" : "Pp"}
                onFocus={e => (e.target.readOnly = true)}
                className="full-width-input"
                timeInputLabel="Время:"
                showTimeInput={inputProps.type === "datetime"}
                shouldCloseOnSelect={false}
                isClearable
                onChange={x => {
                  mutators.setDate({
                    name: inputProps.title,
                    value: x
                  });
                }}
              />
            )}
            {meta.touched && meta.error && (
              <span className="error">{meta.error}</span>
            )}
          </>
        )}
      />
    )}
  </InputWrapper>
);
