const fields = [
  {
    title: "Email",
    type: "email",
    validation: {
      regexp: /^.+@.+\..+$/,
      error: "Incorrect Email!"
    }
  },
  {
    title: "Full Name",
    type: "string",
    validation: {
      regexp: /^\S{4,48}$/,
      error: "Incorrect First Name!"
    }
  },
  {
    title: "Date of Birth",
    type: "date"
  },
  {
    title: "Time of Event",
    type: "datetime"
  },
  {
    title: "Some Text",
    type: "text"
  },
  {
    title: "Some Password",
    type: "password"
  },
  {
    title: "Some Number",
    type: "number"
  },
  {
    title: "Some Selects",
    type: "select",
    options: [
      { text: "option_2_text", value: "option_2_value" },
      { text: "option_1_text", value: "option_1_value" }
    ]
  },
  {
    title: "Some Checkbox",
    type: "checkbox"
  },
  {
    title: "Some Radio",
    type: "radio",
    options: [
      { text: "option_1_text", value: "select_option_1_value" },
      { text: "option_2_text", value: "option_2_value" }
    ]
  }
];

export default fields;
