import React from "react";

export const Select = ({ options, title, input }) => (
  <select name={title} {...input}>
    <option></option>
    {options.map((option, i) => (
      <option key={i} value={option.value}>
        {option.text}
      </option>
    ))}
  </select>
);
