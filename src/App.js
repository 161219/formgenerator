import React from "react";
import { Form } from "react-final-form";
import { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "moment/locale/ru";
import ru from "date-fns/locale/ru";
import "./App.scss";
import { MyField } from "./components/field";
import fields from "./components/fields";
import onSubmit from "./components/on_submit";
registerLocale("ru", ru);

const App = () => (
  <Form
    onSubmit={onSubmit}
    mutators={{
      setDate: (args, state, utils) => {
        utils.changeValue(state, args[0].name, () => args[0].value);
      }
    }}
    render={({ handleSubmit, values, form }) => (
      <div className="form-wrapper">
        <form onSubmit={handleSubmit}>
          <>
            {fields.map((inputProps, i) => (
              <MyField
                inputProps={inputProps}
                mutators={form.mutators}
                key={i}
              />
            ))}
          </>
          <>
            <button type="submit">Отправить</button>
          </>
          {/* <>
            <pre>{JSON.stringify(values, 2, 2)}</pre>
          </> */}
        </form>
      </div>
    )}
  />
);

export default App;
